from os import error
import os
from django.db import models
import json
from numpy.core.fromnumeric import around, partition
# from keras.engine import sequential
from pandas.core import algorithms
from pandas.core.arraylike import array_ufunc
from sklearn import metrics
from sklearn.metrics.pairwise import _argmin_min_reduce
from sympy.core.numbers import One, comp
import pandas as pd
import numpy as np
from sklearn.metrics import mean_absolute_percentage_error,r2_score,mean_squared_error, accuracy_score, recall_score, f1_score, confusion_matrix, precision_score, mean_absolute_error,roc_curve,auc,classification_report
from scipy import interp
from sklearn.cross_decomposition import PLSRegression
from sklearn.ensemble import GradientBoostingRegressor,RandomForestClassifier,RandomForestRegressor,AdaBoostClassifier,AdaBoostRegressor
from sklearn.linear_model import Lasso,Ridge,RidgeCV,LinearRegression, BayesianRidge, SGDRegressor,RidgeClassifier,LogisticRegression
from sklearn.gaussian_process import GaussianProcessRegressor,GaussianProcessClassifier
from sklearn.neighbors import KNeighborsRegressor,KNeighborsClassifier
from sklearn.svm import SVR,SVC
from sklearn.neural_network import MLPRegressor,MLPClassifier
from sklearn.tree import DecisionTreeRegressor,DecisionTreeClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.cluster import KMeans,MiniBatchKMeans,AffinityPropagation,MeanShift,SpectralClustering,AgglomerativeClustering,DBSCAN,OPTICS,Birch
from sklearn.model_selection import StratifiedKFold,train_test_split,KFold
from sklearn.preprocessing import MinMaxScaler,OneHotEncoder,OrdinalEncoder, label_binarize
from imblearn.over_sampling import SMOTE
# import copy
# import math
from math import atanh, sqrt
from garudaAPI.custom_class import ANN,unique,LabelBinarizer2, save_models,load_models
import joblib

from sympy.utilities.lambdify import MPMATH_DEFAULT, _module_present
from enum import Enum, Flag

# Create your models here.


algorithm_list = {"Regression":[LinearRegression, Lasso, RidgeCV,Ridge,MLPRegressor,KNeighborsRegressor,GaussianProcessRegressor,
                                PLSRegression,GradientBoostingRegressor,DecisionTreeRegressor,RandomForestRegressor,AdaBoostRegressor,
                                SVR,SGDRegressor,BayesianRidge,ANN],
                    "Classification":[GaussianProcessClassifier,SVC,KNeighborsClassifier,RidgeClassifier,AdaBoostClassifier,LogisticRegression,
                                    DecisionTreeClassifier,RandomForestClassifier,QuadraticDiscriminantAnalysis,GaussianNB,ANN],
                    "Clustering": [KMeans,MiniBatchKMeans,AffinityPropagation,MeanShift,SpectralClustering,AgglomerativeClustering,DBSCAN,OPTICS,Birch],
                    }


#bikin function get_params ditampung di variabel sebagai array, lalu cek key apakah ada.

class Regression(Enum):
    LinearRegression = 0
    Lasso = 1
    RidgeCV = 2
    Ridge = 3
    MLPRegressor =4
    KNeighborsRegressor =5
    GaussianProcessRegressor =6
    PLSRegression =7
    GradientBoostingRegressor =8
    DecisionTreeRegressor =9
    RandomForestRegressor =10
    AdaBoostRegressor =11
    SVR =12
    SGDRegressor =13
    BayesianRidge =14
    ANN = 15

class Classification(Enum):
    GaussianProcessClassifier = 0
    SVC = 1
    KNeighborsClassifier = 2
    RidgeClassifier = 3
    AdaBoostClassifier = 4 
    LogisticRegression = 5
    DecisionTreeClassifier = 6
    RandomForestClassifier =  7
    QuadraticDiscriminantAnalysis = 8
    GaussianNB = 9
    ANN = 10

class Clustering(Enum):
    KMeans = 0
    MiniBatchKMeans = 1
    AffinityPropagation = 2
    MeanShift = 3
    SpectralClustering = 4
    AgglomerativeClustering = 5
    DBSCAN = 6
    OPTICS = 7
    Birch = 8

def N_class(list1):
    x = np.array(list1)
    return list(np.unique(x))


def cross_validation1(model,cv,algorithm_problem, training_size, X,y, auto_preprocessing,scaler_y):
    kfold = KFold(n_splits= int(cv["splits"]), random_state=1, shuffle=True)
    if algorithm_problem == "Regression":
        MSE = []
        MAPE = []
        RMSE = []
        MAE = []
        R2 = [] 
        folds = 1
        df_metrics = pd.DataFrame(columns = ["y_actual","y_pred","MAE","MSE","MAPE","Label"])
        """ LOOPING UNTUK MELAKUKAN CROSS VALIDATION """
        for train, test in kfold.split(X, y):

            model.fit(X[train],y[train])
            model_test_predict = model.predict(X[test])

            if auto_preprocessing:
                model_test_predict = scaler_y.inverse_transform(np.array(model_test_predict).reshape(-1,1))
                y[test] = scaler_y.inverse_transform(np.array(y[test]).reshape(-1,1))

            R2.append(r2_score(y[test], model_test_predict))
            # print("R2 Score \n", R2)
            MAE.append(mean_absolute_error(y[test],model_test_predict))
            MAPE.append(mean_absolute_percentage_error(y[test],model_test_predict))
            MSE.append(mean_squared_error(y[test], model_test_predict))
            RMSE.append(sqrt(mean_squared_error(y[test], model_test_predict)))

            abs_error = []
            squared_error = []
            percentage_error = []
            fold = []

            for true,pred in zip(list(np.array(y[test]).flatten()),model_test_predict.flatten()):
                mae_ = abs(true - pred)
                mse_ = abs((true - pred)**2)
                mape_ = mae_/true*100
                abs_error.append(np.around(mae_,2))
                squared_error.append(np.around(mse_,2))
                percentage_error.append(np.around(mape_,2))
                fold.append(folds)
            df_metrics1 = pd.DataFrame({"y_actual":list(np.array(y[test]).flatten()),
                    "y_pred":list(np.array(model_test_predict).flatten()),
                    "MAE":abs_error,
                    "MSE":squared_error,
                    "MAPE":percentage_error,
                    "Label":fold})
            df_metrics = df_metrics.append(df_metrics1,ignore_index = True)
            folds += 1

        """ DATASPLIT UNTUK TRAINING NON CROSS VALIDATION """
        X_train0, X_test, Y_train0, y_test = train_test_split(X, y, test_size= 1-training_size/100, random_state= 42)

        """ TRAINING UNTUK NON CROSS VALIDATION """
        model.fit(X_train0,Y_train0)

        """ PREDICTION UNTUK NON CROSS VALIDATION """
        model_train_predict = model.predict(X_train0)
        model_test_predict = model.predict(X_test) 
        
        return X_train0, X_test, Y_train0, y_test,model_train_predict,model_test_predict,df_metrics, MAE,MAPE,MSE,RMSE,R2
    # elif algorithm_problem == "Classification":
    #     accuracy = []
    #     recall = []
    #     precision = []
    #     f1 = []
    #     con_matrix = []
    #     fpr = {}
    #     tpr ={}
    #     roc_auc ={}
    #     fold = 0
    #     # df_target = pd.DataFrame(y)
    #     # imbalanced = set(df_target.value_counts())
    #     # if len(imbalanced) != 1:
    #     #     # SMOTE = SMOTE()
    #     #     X,y = SMOTE().fit_resample(X, y)
    #     """ LOOPING UNTUK MELAKUKAN CROSS VALIDATION """
    #     for train, test in kfold.split(X, y):
    #         print(X[train],y[train])
    #         model.fit(X[train],y[train])
    #         model_test_predict = model.predict(X[test])
    #         # if auto_preprocessing:
    #             # model_train_predict = scaler_y.inverse_transform(np.array(model_train_predict).reshape(-1,1))
    #         #     model_test_predict = scaler_y.inverse_transform(np.array(model_test_predict).reshape(-1,1))
    #         #     y[test] = scaler_y.inverse_transform(np.array(y[test]).reshape(-1,1))
    #         # print(model_test_predict,y[test])
    #         acc = accuracy_score(y[test], model_test_predict)
    #         rec = recall_score(y[test], model_test_predict,average=None).tolist()
    #         prec = precision_score(y[test], model_test_predict,average=None).tolist()
    #         f1_ = f1_score(y[test], model_test_predict,average=None).tolist()
    #         con = confusion_matrix(y[test], model_test_predict).tolist()
    #         accuracy.append(acc)
    #         recall.append(rec)
    #         precision.append(prec)
    #         f1.append(f1_)
    #         con_matrix.append(con)
            
    #         fpr_i = dict()
    #         tpr_i = dict()
    #         roc_auc_i = dict()
    #         pred = model.predict_proba(X[test])
    #         # print("STRING", y[test])
    #         # print("LEN", len(np.unique(y[test])))
    #         y_binarize = LabelBinarizer2().fit_transform(y[test])
    #         # print("BINARIZE",y_binarize)
    #         for i in range(len(np.unique(y[test]))):
    #             fpr_i[i], tpr_i[i], _ = roc_curve(y_binarize[:, i], pred[:, i])
    #             roc_auc_i[i] = auc(fpr_i[i], tpr_i[i])


    #         # Compute micro-average ROC curve and ROC area
    #         fpr_i["micro"], tpr_i["micro"], _ = roc_curve(y_binarize.ravel(), pred.ravel())
    #         roc_auc_i["micro"] = auc(fpr_i["micro"], tpr_i["micro"])

    #         # First aggregate all false positive rates
    #         all_fpr_i = np.unique(np.concatenate([fpr_i[i] for i in range(len(np.unique(y[test])))]))

    #         # Then interpolate all ROC curves at this points
    #         mean_tpr_i = np.zeros_like(all_fpr_i)
    #         for i in range(len(np.unique(y[test]))):
    #             mean_tpr_i += np.interp(all_fpr_i, fpr_i[i], tpr_i[i])

    #         # Finally average it and compute AUC
    #         mean_tpr_i /= len(np.unique(y[test]))

    #         fpr_i["macro"] = all_fpr_i
    #         tpr_i["macro"] = mean_tpr_i
    #         roc_auc_i["macro"] = auc(fpr_i["macro"], tpr_i["macro"])
    #         for i,z in fpr_i.items():
    #             fpr_i[i] = z.tolist()
    #         for i,z in tpr_i.items():
    #             tpr_i[i] = z.tolist()
    #         tpr["fold "+str(fold)] = tpr_i
    #         fpr["fold "+str(fold)] = fpr_i
    #         roc_auc["fold "+str(fold)] = roc_auc_i

    #         fold += 1

    #     """ DATASPLIT UNTUK TRAINING NON CROSS VALIDATION """
    #     X_train0, X_test, Y_train0, y_test = train_test_split(X, y, test_size= 1-training_size/100, random_state= 42)
        
    #     """ TRAINING UNTUK NON CROSS VALIDATION """
    #     model.fit(X_train0,Y_train0)
        
    #     """ PREDICTION UNTUK NON CROSS VALIDATION """
    #     model_train_predict = model.predict(X_train0)
    #     model_test_predict = model.predict(X_test)

    #     return X_train0, X_test, Y_train0, y_test,model_train_predict,model_test_predict,accuracy, recall, precision, f1, con_matrix, tpr,fpr,roc_auc

    else:
        """JIKA ALGORITMA TIDAK MEMPUNYAI CROSS VALIDATION"""
        error_message = "Invalid Cross Validation"
        return error_message       
        


def cross_validation2(model,cv,algorithm_problem, training_size, X,y,):
    kfold = StratifiedKFold(n_splits= int(cv["splits"]), random_state=1, shuffle=True)
    if algorithm_problem == "Classification":
        accuracy = []
        recall = []
        precision = []
        f1 = []
        con_matrix = []
        dict_metrics = {}
        fpr = {}
        tpr ={}
        roc_auc ={}
        fold = 0
        # df_target = pd.DataFrame(y)
        # imbalanced = set(df_target.value_counts())
        # if len(imbalanced) != 1:
        #     # SMOTE = SMOTE()
        #     X,y = SMOTE().fit_resample(X, y)
        """ LOOPING UNTUK MELAKUKAN CROSS VALIDATION """
        for train, test in kfold.split(X, y):
            print(X[train],y[train])
            model.fit(X[train],y[train])
            model_test_predict = model.predict(X[test])
            # if auto_preprocessing:
                # model_train_predict = scaler_y.inverse_transform(np.array(model_train_predict).reshape(-1,1))
            #     model_test_predict = scaler_y.inverse_transform(np.array(model_test_predict).reshape(-1,1))
            #     y[test] = scaler_y.inverse_transform(np.array(y[test]).reshape(-1,1))
            # print(model_test_predict,y[test])
            # acc = accuracy_score(y[test], model_test_predict)
            # rec = recall_score(y[test], model_test_predict,average=None).tolist()
            # prec = precision_score(y[test], model_test_predict,average=None).tolist()
            # f1_ = f1_score(y[test], model_test_predict,average=None).tolist()
            con = confusion_matrix(y[test], model_test_predict).tolist()
            # accuracy.append(acc)
            # recall.append(rec)
            # precision.append(prec)
            # f1.append(f1_)
            con_matrix.append(con)
            
            dict_i = classification_report(y[test], model_test_predict, output_dict=True, labels=N_class(y))
            fpr_i = dict()
            tpr_i = dict()
            roc_auc_i = dict()
            pred = model.predict_proba(X[test])
            # print("STRING", y[test])
            # print("LEN", len(np.unique(y[test])))
            y_binarize = LabelBinarizer2().fit_transform(y[test])
            # print("BINARIZE",y_binarize)
            for i in range(len(np.unique(y[test]))):
                fpr_i[i], tpr_i[i], _ = roc_curve(y_binarize[:, i], pred[:, i])
                roc_auc_i[i] = auc(fpr_i[i], tpr_i[i])


            # Compute micro-average ROC curve and ROC area
            fpr_i["micro"], tpr_i["micro"], _ = roc_curve(y_binarize.ravel(), pred.ravel())
            roc_auc_i["micro"] = auc(fpr_i["micro"], tpr_i["micro"])

            # First aggregate all false positive rates
            all_fpr_i = np.unique(np.concatenate([fpr_i[i] for i in range(len(np.unique(y[test])))]))

            # Then interpolate all ROC curves at this points
            mean_tpr_i = np.zeros_like(all_fpr_i)
            for i in range(len(np.unique(y[test]))):
                mean_tpr_i += np.interp(all_fpr_i, fpr_i[i], tpr_i[i])

            # Finally average it and compute AUC
            mean_tpr_i /= len(np.unique(y[test]))

            fpr_i["macro"] = all_fpr_i
            tpr_i["macro"] = mean_tpr_i
            roc_auc_i["macro"] = auc(fpr_i["macro"], tpr_i["macro"])
            for i,z in fpr_i.items():
                fpr_i[i] = z.tolist()
            for i,z in tpr_i.items():
                tpr_i[i] = z.tolist()
            tpr["fold "+str(fold)] = tpr_i
            fpr["fold "+str(fold)] = fpr_i
            roc_auc["fold "+str(fold)] = roc_auc_i
            dict_metrics["fold "+str(fold)] = dict_i

            fold += 1

        """ DATASPLIT UNTUK TRAINING NON CROSS VALIDATION """
        X_train0, X_test, Y_train0, y_test = train_test_split(X, y, test_size= 1-training_size/100, random_state= 42)
        
        """ TRAINING UNTUK NON CROSS VALIDATION """
        model.fit(X_train0,Y_train0)
        
        """ PREDICTION UNTUK NON CROSS VALIDATION """
        model_train_predict = model.predict(X_train0)
        model_test_predict = model.predict(X_test)

        # return X_train0, X_test, Y_train0, y_test,model_train_predict,model_test_predict,accuracy, recall, precision, f1, con_matrix, tpr,fpr,roc_auc
        return X_train0, X_test, Y_train0, y_test,model_train_predict,model_test_predict,dict_metrics,con_matrix, tpr,fpr,roc_auc

    else:
        """JIKA ALGORITMA TIDAK MEMPUNYAI CROSS VALIDATION"""
        error_message = "Invalid Cross Validation"
        return error_message       
        


class ModelTestApi:
    def __init__(self,company_name):
        self.company_name = company_name
    
    def build_model(self, company_name, n_number):
        self.is_success = False
        self.company_name = company_name
        self.number = n_number
        list_output = []
        for i in range(int(self.number)):
            list_output.append(self.company_name)

        self.data_dict = {
            'List Output' : list_output
        }
        self.is_success = True
        return self



class AlgorithmSelection:
    def __init__(self, project_name):
        self.project_name = project_name
    
    def build_algorithm(self, data_sources, predictor,target,training_size, algorithm, algorithm_parameter,cross_val,auto_preprocessing):

        directory = self.project_name
        # parent_dir = r"C:\Users\midat\\Downloads\midat_api\jiwa.machine.learning\garudaAPI"
        parent_dir = os.getcwd()
        print(parent_dir)
        path = os.path.join(parent_dir, directory)
        print(path)
        os.makedirs(path,exist_ok=True)

        """ Buat Pandas DataFrame """
        df_data_source = pd.DataFrame.from_dict(data_sources) #belum tentu berhasil
        

        """ Check error from parameter """
        if algorithm_list[algorithm["problem"]][algorithm["index"]] == ANN:
            model = algorithm_list[algorithm["problem"]][algorithm["index"]](df_target = df_data_source[target],problem= algorithm["problem"], **algorithm_parameter)
        else:
            model = algorithm_list[algorithm["problem"]][algorithm["index"]](**algorithm_parameter)
        
        """ CHECK IF THERE IS PARAMETER THAT DOESN'T BELONG TO ALGORITHM """
        set1 = set(algorithm_parameter.keys()) 
        set2 = set(model.get_params()) 
        if set1.difference(set2):
            self.error_message = "Invalid Machine Learning Parameter"
            return self
        
        """ CHECK FOR DUPLICATE DATA AND MISSING VALUES """
        if auto_preprocessing:
            df_data_source = df_data_source.drop_duplicates(keep="first").reset_index()
            df_data_source = df_data_source.dropna().reset_index()

        """ MODEL BUILD FOR CLASSIFICATION AND REGRESSION """
        if algorithm["problem"] == "Regression" or algorithm["problem"] == "Classification":
            df_predictor = df_data_source.loc[:, [item for item in predictor]]
            df_target = df_data_source[target]

            """ ENCODE DATASET FOR COMPUTATION """
            if any([isinstance(item, str) for item in df_target]):
                encoder = OrdinalEncoder()
                encode = encoder.fit_transform(df_target.values.reshape(-1, 1))
                df_target = pd.DataFrame(encode.tolist(), columns = [target])
                joblib_file = os.path.join(path,"encode.joblib")
                joblib.dump(encode, joblib_file)

            list_column = df_predictor.columns
            for i in list_column:
                if any([isinstance(item, str) for item in df_predictor[i]]):
                    encoder1 = OneHotEncoder(sparse = False).fit_transform(df_predictor[[i]])
                    d2 = pd.DataFrame(encoder1.tolist(), columns=[str(item) for item in df_predictor[i].unique().tolist()]).reset_index(drop=True)
                    d1 = df_predictor.copy().reset_index(drop=True)
                    df_predictor = pd.concat([d1,d2],axis = 1)
                    df_predictor = df_predictor.drop(columns=[i])
                    joblib_file = os.path.join(path,"OneHotEncoder.joblib")
                    joblib.dump(encoder1, joblib_file)
            X_scaled = df_predictor.values
            Y_scaled = df_target.values

            """ IMBALANCE DATASET  PROCESSING"""
            if algorithm["problem"] == "Classification" and auto_preprocessing:
                imbalanced = set(df_target.value_counts())
                if len(imbalanced) != 1:
                    scale = SMOTE()
                    X_scaled,Y_scaled = scale.fit_resample(X_scaled, Y_scaled)

            """ FEATURES SCALLING """
            if auto_preprocessing:
                scaler_X = MinMaxScaler(feature_range=(0,1))
                scaler_y = MinMaxScaler(feature_range=(0,1))
                X_scaled = scaler_X.fit_transform(X_scaled)
                if algorithm["problem"] == "Regression":
                    Y_scaled = scaler_y.fit_transform(Y_scaled.reshape(-1,1))
                    joblib_file = os.path.join(path,"scaler_y.joblib")
                    joblib.dump(scaler_y, joblib_file)
                
                joblib_file = os.path.join(path,"scaler_X.joblib")
                joblib.dump(scaler_X, joblib_file)
            else:
                scaler_X = None
                scaler_y = None
                
            
            """ MODEL BUILDING FOR CROSS VALIDATION """
            if ('splits' in cross_val):
                if algorithm["problem"] == "Regression":
                    X_train0,X_test,Y_train0,y_test,model_train_predict,model_test_predict,df_metrics, mae,mape,mse,rmse,score = cross_validation1(model,cross_val,algorithm["problem"], training_size,X_scaled,Y_scaled,auto_preprocessing,scaler_y)
                    # print(df_metrics)
                    print(df_metrics.to_dict())
                    """ INVERSE TRANSFORM DATASET """
                    if auto_preprocessing:
                        model_train_predict = scaler_y.inverse_transform(np.array(model_train_predict).reshape(-1,1))
                        model_test_predict = scaler_y.inverse_transform(np.array(model_test_predict).reshape(-1,1))
                        y_test = scaler_y.inverse_transform(np.array(y_test).reshape(-1,1))
                        Y_train0 = scaler_y.inverse_transform(np.array(Y_train0).reshape(-1,1))
                        # print("\n AFTER", y_test)
                        try:
                            encoder
                            df_target = df_data_source[target]
                        except NameError:
                            encoder = None
                        try:
                            encoder1
                            df_predictor = df_data_source.loc[:, [item for item in predictor]]
                        except NameError:
                            encoder = None
                    
                    self.dict_eval={"score":score,
                                    "mae" : mae,
                                    "mape": mape,
                                    "mse" : mse,
                                    "rmse": rmse
                                    }
                    dict_x_train = {}
                    col_num = 0
                    for i in predictor:
                        dict_x_train[i] = list(X_train0[:,col_num])
                        col_num = col_num + 1
                    dict_x_test = {}
                    col_num = 0
                    for i in predictor:
                        dict_x_test[i] = list(X_test[:,col_num])
                        col_num = col_num + 1
                    self.df_predictor = df_predictor.to_dict()
                    self.df_target = df_target.to_dict()
                    self.df_metric = df_metrics.to_dict()
                    self.model_name = algorithm
                    self.X_train = dict_x_train
                    self.y_train = Y_train0.tolist()
                    self.X_test = dict_x_test
                    self.y_test = y_test.tolist()
                    self.model_train_predict = model_train_predict.tolist()
                    self.model_test_predict = model_test_predict.tolist()
                    

                elif algorithm["problem"] == "Classification":
                    # X_train0,X_test,Y_train0, y_test, model_train_predict,model_test_predict,accuracy,recall,precision,f1,con_matrix,tpr,fpr,roc_auc = cross_validation2(model,cross_val,algorithm["problem"], training_size,X_scaled,Y_scaled)
                    X_train0,X_test,Y_train0, y_test, model_train_predict,model_test_predict,dict_metrics,con_matrix,tpr,fpr,roc_auc = cross_validation2(model,cross_val,algorithm["problem"], training_size,X_scaled,Y_scaled)
                    
                    if auto_preprocessing:
                        try:
                            encoder
                            df_target = df_data_source[target]
                        except NameError:
                            encoder = None
                        try:
                            encoder1
                            df_predictor = df_data_source.loc[:, [item for item in predictor]]
                        except NameError:
                            encoder = None
                    self.dict_eval={
                                    # "accuracy":accuracy,
                                    # "recall" : recall,
                                    # "precision" : precision,
                                    # "f1": f1,
                                    "confusion matrix":con_matrix,
                                    "False Positive Rate": fpr,
                                    "True Positive Rate": tpr,
                                    "Receiver Operating Characteristic": roc_auc
                                    }
                    dict_x_train = {}
                    col_num = 0
                    for i in predictor:
                        dict_x_train[i] = list(X_train0[:,col_num])
                        col_num = col_num + 1
                    dict_x_test = {}
                    col_num = 0
                    for i in predictor:
                        dict_x_test[i] = list(X_test[:,col_num])
                        col_num = col_num + 1

                    self.df_predictor = df_predictor.to_dict()
                    self.df_target = df_target.to_dict()
                    self.df_metric = dict_metrics
                    self.model_name = algorithm
                    self.X_train = dict_x_train
                    self.y_train = Y_train0.tolist()
                    self.X_test = dict_x_test
                    self.y_test = y_test.tolist()
                    if auto_preprocessing and algorithm["problem"] == "Classification":
                        try:
                            encoder
                            print(encoder)
                            encoder = OrdinalEncoder()
                            encode = encoder.fit_transform(df_target.values.reshape(-1, 1))
                            model_test_predict = encoder.inverse_transform(model_test_predict.reshape(-1, 1))
                            model_train_predict = encoder.inverse_transform(model_train_predict.reshape(-1, 1))
                        except NameError:
                            model_test_predict = model_test_predict
                            model_train_predict = model_train_predict
                    self.model_train_predict = model_train_predict.tolist()
                    self.model_test_predict = model_test_predict.tolist()
                
            else:
                """ MODEL BUILDING FOR NON CROSSS VALIDATION """

                """ Dataset Split """
                X_train0, X_test, Y_train0, y_test = train_test_split(X_scaled, Y_scaled, test_size= 1-training_size/100, random_state= 42)

                """ TRAINING UNTUK NON CROSS VALIDATION """
                history = model.fit(X_train0,Y_train0)

                
                """ PREDICTION UNTUK NON CROSS VALIDATION """
                model_train_predict = model.predict(X_train0)
                model_test_predict = model.predict(X_test)
                
                """ INVERSE TRANSFORM DATASET """
                if auto_preprocessing and algorithm["problem"] == "Regression":
                    model_test_predict = scaler_y.inverse_transform(np.array(model_test_predict).reshape(-1,1))
                    model_train_predict = scaler_y.inverse_transform(np.array(model_train_predict).reshape(-1,1))
                    # print("BEFORE", y_test)
                    y_test = scaler_y.inverse_transform(np.array(y_test).reshape(-1,1))
                    Y_train0 = scaler_y.inverse_transform(np.array(Y_train0).reshape(-1,1))
                    # print("\n AFTER", list(Y_train0.flatten()))
                    # print("\n AFTER", model_train_predict)
                    try:
                        encoder
                        df_target = df_data_source[target]
                    except NameError:
                        encoder = None
                    try:
                        encoder1
                        df_predictor = df_data_source.loc[:, [item for item in predictor]]
                    except NameError:
                        encoder = None
                
                """ INVERSE TRANSFORM DATASET """
                if auto_preprocessing and algorithm["problem"] == "Classification":
                    try:
                        encoder
                        df_target = df_data_source[target]
                    except NameError:
                        encoder = None
                    try:
                        encoder1
                        df_predictor = df_data_source.loc[:, [item for item in predictor]]
                    except NameError:
                        encoder = None

                """ MODEL EVALUATION """
                if algorithm["problem"] == "Regression":
                    # ubah label categorical jadi numerikal
                    mae = []
                    mse = []
                    mape = []
                    label = []
                    for true,pred in zip(Y_train0,model_train_predict):
                        mae_ = abs(true - pred)
                        mse_ = abs((true - pred)**2)
                        mape_ = mae_/true*100
                        mae.append(mae_)
                        mse.append(mse_)
                        mape.append(mape_)
                        label.append(0)

                    df2 = pd.DataFrame({"y_actual":list(np.array(Y_train0).flatten()),
                                        "y_pred":list(np.array(model_train_predict).flatten()),
                                        "MAE":list(np.array(mae).flatten()),
                                        "MSE":list(np.array(mse).flatten()),
                                        "MAPE":list(np.array(mape).flatten()),
                                        "label":list(np.array(label).flatten())})
                    mae = []
                    mse = []
                    mape = []
                    label = []
                    for true,pred in zip(y_test,model_test_predict):
                        mae_ = abs(true - pred)
                        mse_ = abs((true - pred)**2)
                        mape_ = mae_/true*100
                        mae.append(mae_)
                        mse.append(mse_)
                        mape.append(mape_)
                        label.append(1)

                    df1 = pd.DataFrame({"y_actual":list(np.array(y_test).flatten()),
                                        "y_pred":list(np.array(model_test_predict).flatten()),
                                        "MAE":list(np.array(mae).flatten()),
                                        "MSE":list(np.array(mse).flatten()),
                                        "MAPE":list(np.array(mape).flatten()),
                                        "label":list(np.array(label).flatten())})   
                    df_metrics = df1.append(df2,ignore_index = True)

                    # print(df_metrics)

                    score = r2_score(y_test, model_test_predict)
                    mae = mean_absolute_error(y_test,model_test_predict)
                    mape = mean_absolute_percentage_error(y_test,model_test_predict)
                    mse = mean_squared_error(y_test, model_test_predict)
                    rmse = sqrt(mse)
                    self.dict_eval={"score":score,
                                    "mae" : mae,
                                    "mape": mape,
                                    "mse" : mse,
                                    "rmse": rmse
                                    }
                    dict_x_train = {}
                    col_num = 0
                    for i in predictor:
                        dict_x_train[i] = list(X_train0[:,col_num])
                        col_num = col_num + 1
                    dict_x_test = {}
                    col_num = 0
                    for i in predictor:
                        dict_x_test[i] = list(X_test[:,col_num])
                        col_num = col_num + 1
                    self.df_predictor = df_predictor.to_dict()
                    self.df_target = df_target.to_dict()
                    self.df_metric = df_metrics.to_dict()
                    self.model_name = algorithm
                    self.X_train = dict_x_train
                    self.y_train = Y_train0.tolist()
                    self.X_test = dict_x_test
                    self.y_test = y_test.tolist()
                    self.model_train_predict = model_train_predict.tolist()
                    self.model_test_predict = model_test_predict.tolist()
                    if algorithm_list[algorithm["problem"]][algorithm["index"]] == ANN:
                        self.history_loss = history.history.history['loss']
                        self.history_val = history.history.history['val_loss']

                elif algorithm["problem"] == "Classification":
                    # accuracy = accuracy_score(y_test, model_test_predict)
                    # recall = recall_score(y_test, model_test_predict,average=None).tolist()
                    # precision = precision_score(y_test, model_test_predict,average=None).tolist()
                    # f1 = f1_score(y_test, model_test_predict,average=None).tolist()
                    con_matrix = confusion_matrix(y_test, model_test_predict).tolist()
                    # print(y_test)
                    # print(model_test_predict)
                    # print(N_class(Y_scaled))
                    dict_metrics = classification_report(y_test, model_test_predict, output_dict=True, labels=N_class(Y_scaled))
                    fpr = dict()
                    tpr = dict()
                    roc_auc = dict()
                    pred = model.predict_proba(X_test)
                    # print(y_test)
                    y_binarize = LabelBinarizer2().fit_transform(y_test)
                    # y_binarize = label_binarize(y_test, classes=list(range(0,unique(df_target))))
                    for i in range(unique(df_target)):
                        fpr[i], tpr[i], _ = roc_curve(y_binarize[:, i], pred[:, i])
                        roc_auc[i] = auc(fpr[i], tpr[i])


                    # Compute micro-average ROC curve and ROC area
                    fpr["micro"], tpr["micro"], _ = roc_curve(y_binarize.ravel(), pred.ravel())
                    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

                    # First aggregate all false positive rates
                    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(unique(df_target))]))

                    # Then interpolate all ROC curves at this points
                    mean_tpr = np.zeros_like(all_fpr)
                    for i in range(unique(df_target)):
                        mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])

                    # Finally average it and compute AUC
                    mean_tpr /= unique(df_target)

                    fpr["macro"] = all_fpr
                    tpr["macro"] = mean_tpr
                    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])
                    for i,z in fpr.items():
                        fpr[i] = z.tolist()
                    for i,z in tpr.items():
                        tpr[i] = z.tolist()
                    
                    # print(fpr)
                    # print(tpr)
                    # print(roc_auc)

                    self.dict_eval={
                                    # "accuracy":accuracy,
                                    # "recall" : recall,
                                    # "precision" : precision,
                                    # "f1": f1,
                                    "confusion matrix":con_matrix,
                                    "False Positive Rate": fpr,
                                    "True Positive Rate": tpr,
                                    "Receiver Operating Characteristic": roc_auc
                                    }
                    dict_x_train = {}
                    col_num = 0
                    for i in predictor:
                        dict_x_train[i] = list(X_train0[:,col_num])
                        col_num = col_num + 1
                    dict_x_test = {}
                    col_num = 0
                    for i in predictor:
                        dict_x_test[i] = list(X_test[:,col_num])
                        col_num = col_num + 1

                    self.df_predictor = df_predictor.to_dict()
                    self.df_target = df_target.to_dict()
                    self.model_name = algorithm
                    self.df_metric = dict_metrics
                    self.X_train = dict_x_train
                    self.y_train = Y_train0.tolist()
                    self.X_test = dict_x_test
                    self.y_test = y_test.tolist()
                    if auto_preprocessing and algorithm["problem"] == "Classification":
                        try:
                            encoder
                            print(encoder)
                            encoder = OrdinalEncoder()
                            encode = encoder.fit_transform(df_target.values.reshape(-1, 1))
                            model_test_predict = encoder.inverse_transform(model_test_predict.reshape(-1, 1))
                            model_train_predict = encoder.inverse_transform(model_train_predict.reshape(-1, 1))
                        except NameError:
                            model_test_predict = model_test_predict
                            model_train_predict = model_train_predict
                    self.model_train_predict = model_train_predict.tolist()
                    self.model_test_predict = model_test_predict.tolist()
                    if algorithm_list[algorithm["problem"]][algorithm["index"]] == ANN:
                        self.history_loss = history.history.history['loss']
                        self.history_val = history.history.history['val_loss']

            
            """ MODEL BUILDING FOR CLUSTERING ALGORTIHM """
        elif algorithm["problem"] == "Clustering":
            X = df_data_source.values
            model = algorithm_list[algorithm["problem"]][algorithm["index"]](**algorithm_parameter).fit(X)
            self.data = df_data_source.to_dict()
            self.model_name = algorithm
            self.label= model.labels_.tolist()
            # self.centroids = model.cluster_centers_

        # joblib_file = str(self.project_name) + "." +algorithm["problem"]
        # directory = str(self.project_name)
        # parent_dir = "C:\Users\midat\Downloads\midat_api\jiwa.machine.learning\garudaAPI"
        # path = os.path.join(parent_dir, directory)
        # os.mkdir(path)

        # joblib_file = os.path.join(path,"model.joblib")
        # joblib.dump(model, joblib_file)
        save_models(path, model, algorithm_list[algorithm["problem"]][algorithm["index"]])
        self.is_success = True
        return self


class Prediction:
    def __init__(self,project_name):
        self.project_name = project_name
    
    def predict(self, data_sources, algorithm, predictor, auto_preprocessing):

        df_data_source = pd.DataFrame.from_dict(data_sources)
        
        """ load model """
        path = os.getcwd()
        model_FOLDER = os.path.join(path, self.project_name)
        # model_FILE = os.path.join(model_FOLDER, "model.joblib")
        print(model_FOLDER)
        model = load_models(model_FOLDER,algorithm_list[algorithm["problem"]][algorithm["index"]])
        print(model)

        if algorithm["problem"] == "Regression" or algorithm["problem"] == "Classification":
            df_data_source = df_data_source.loc[:, [item for item in predictor]]
            X = df_data_source.values
            """ load scaler """
            if auto_preprocessing:
                scaler_X_path = os.path.join(model_FOLDER, "scaler_X.joblib")
                scaler_X = joblib.load(scaler_X_path)
                X = scaler_X.fit_transform(X)
                scaler_y_path = os.path.join(model_FOLDER, "scaler_y.joblib")
                isExist = os.path.exists(scaler_y_path)
                if isExist:
                    scaler_y = joblib.load(scaler_y_path)
                       
            y_prediction = model.predict(X)
            # print(y_prediction)
            if isExist:
                y_prediction = scaler_y.inverse_transform(y_prediction)
            y_prediction = y_prediction.tolist()
            self.prediction_data = y_prediction

        elif algorithm["problem"] == "Clustering":
            X = df_data_source.values
            y_prediction = model.predict(X)
            self.prediction_data = y_prediction.tolist()

        self.is_success = True
        return self

